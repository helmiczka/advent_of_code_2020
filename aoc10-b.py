import sys


def multiply_arrangements(omittable_group_size):
    if omittable_group_size == 1:
        return 2
    elif omittable_group_size == 2:
        return 4
    elif omittable_group_size == 3:
        return 7
    return 0


def main(inputfilename):
    adapter_chain = []
    charging_outlet = 0

    omittable_adapters = []

    with open(inputfilename, "r") as f:
        for line in f:
            adapter_chain.append(int(line))

    adapter_chain.append(charging_outlet)
    adapter_chain.sort()

    for i in range(2, len(adapter_chain)):

        jolt_difference = adapter_chain[i] - adapter_chain[i - 2]
        if jolt_difference < 3:
            omittable_adapters.append(adapter_chain[i - 1])

    arrangements = 1
    omittable_group = 1

    for i in range(1, len(omittable_adapters)):
        if omittable_adapters[i] - omittable_adapters[i - 1] == 1:
            omittable_group += 1
        else:
            arrangements *= multiply_arrangements(omittable_group)
            omittable_group = 1
    else:  # last group of adapters
        arrangements *= multiply_arrangements(omittable_group)

    return arrangements


if __name__ == "__main__":
    print(main(sys.argv[1]))
