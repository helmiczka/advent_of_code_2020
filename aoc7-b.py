import sys
import re


def evaluate_paths_to_leaves(node, graph):
    contained = 0

    for edge in graph:
        if edge[1] == 'no other bags.':
            continue
        if node == edge[0]:
            regex_match = re.match(r'(\d+) (.*) (bag)', edge[1])
            amount = regex_match.groups(0)[0]
            inner_bag = regex_match.groups(0)[1]
            amount = int(amount)
            contained += amount + amount * evaluate_paths_to_leaves(inner_bag, graph)

    return contained


def main(inputfilename):
    rules = []
    outer_bag_set = set()

    f = open(inputfilename, "r")

    for line in f:
        outer_bag, contained = line.strip().split(' bags contain ')
        contained = contained.split(', ')
        for bag in contained:
            rules.append((outer_bag, bag))

    contained_bags = evaluate_paths_to_leaves('shiny gold', rules)

    print(contained_bags)
    return contained_bags


if __name__ == "__main__":
    main(sys.argv[1])
