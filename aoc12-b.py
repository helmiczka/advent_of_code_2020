import sys
import math


def manhattan_distance(x, y):
    return abs(x) + abs(y)


# clockwise rotation matrix
# https://en.wikipedia.org/wiki/Rotation_matrix#Direction
def rotate_vector(_x, _y, degrees):
    radians = math.radians(degrees)
    cosine = math.cos(radians)
    sine = math.sin(radians)

    x = round(_x * cosine + _y * sine)
    y = round(-_x * sine + _y * cosine)

    return x, y


def main(inputfilename):
    x = 0
    y = 0
    waypoint_x = 10
    waypoint_y = 1

    with open(inputfilename, "r") as f:
        for line in f:
            line = line.strip()
            number = int(line[1:])

            if line[0] == "N":
                waypoint_y += number
            if line[0] == "S":
                waypoint_y -= number
            if line[0] == "E":
                waypoint_x += number
            if line[0] == "W":
                waypoint_x -= number

            if line[0] == "F":
                x += number * waypoint_x
                y += number * waypoint_y

            if line[0] == "L":
                waypoint_x, waypoint_y = rotate_vector(waypoint_x, waypoint_y, -number)
            if line[0] == "R":
                waypoint_x, waypoint_y = rotate_vector(waypoint_x, waypoint_y, number)

    return manhattan_distance(x, y)


if __name__ == "__main__":
    print(main(sys.argv[1]))
