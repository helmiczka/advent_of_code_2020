import sys


def main(inputfilename):
    x = 0
    y = 0
    trees = 0

    f = open(inputfilename, "r")

    # next(f)
    for line in f:
        line = line.strip()
        if line[x] == "#":
            trees += 1
        x += 3
        if x >= len(line):
            x -= len(line)

    print(trees)
    return trees


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1])
