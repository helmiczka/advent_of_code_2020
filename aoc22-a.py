import sys


def read_player(f):
    deck = []

    f.readline()
    while True:
        line = f.readline().strip()
        if not line:
            break
        else:
            deck.append(int(line))

    return deck


def combat(p1, p2):
    while len(p1) > 0 and len(p2) > 0:
        if p1[0] > p2[0]:
            p1.append(p1[0])
            p1.append(p2[0])
            p1.pop(0)
            p2.pop(0)
        else:
            p2.append(p2[0])
            p2.append(p1[0])
            p1.pop(0)
            p2.pop(0)
    return p1, p2


def winning_score(winning_deck):
    winning_score = 0
    multiplier = 1
    winning_deck.reverse()
    for card in winning_deck:
        winning_score += card * multiplier
        multiplier += 1
    return winning_score


def main(inputfilename):
    p1 = []
    p2 = []

    with open(inputfilename, "r") as f:
        p1 = read_player(f)
        p2 = read_player(f)

    p1, p2 = combat(p1, p2)

    winning_deck = p1 + p2

    return winning_score(winning_deck)


if __name__ == "__main__":
    print(main(sys.argv[1]))
