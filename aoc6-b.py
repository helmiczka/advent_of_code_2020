import sys


def main(inputfilename):
    questions = 0

    f = open(inputfilename, "r")

    question_setlist = []

    for line in f:
        line = line.strip()
        if len(line) == 0:
            if question_setlist:
                questions += len(set.intersection(*question_setlist))
            question_setlist = []
        else:
            current_question_set = set()
            for answer in line:
                current_question_set.add(answer)
            question_setlist.append(current_question_set)
    else:  # process last group which may not have a newline
        if question_setlist:
            questions += len(set.intersection(*question_setlist))

    print(questions)
    return questions


if __name__ == "__main__":
    main(sys.argv[1])
