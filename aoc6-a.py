import sys


def main(inputfilename):
    questions = 0

    f = open(inputfilename, "r")

    current_question_set = set()

    for line in f:
        line = line.strip()
        if len(line) == 0:
            questions += len(current_question_set)
            current_question_set = set()
        else:
            for answer in line:
                current_question_set.add(answer)
    else:  # process last group which may not have a newline
        questions += len(current_question_set)

    print(questions)
    return questions


if __name__ == "__main__":
    main(sys.argv[1])
