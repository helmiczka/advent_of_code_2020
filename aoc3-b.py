import sys


def count_trees(slope, _x, _y):
    x = 0
    y = 0
    trees = 0

    while y < len(slope):
        if slope[y][x] == "#":
            trees += 1
        x += _x
        x = x % len(slope[y])
        y += _y

    return trees


def main(inputfilename):
    slope = []
    f = open(inputfilename, "r")
    for line in f:
        slope_slice = []
        for char in line.strip():
            slope_slice += char
        slope.append(slope_slice)

    traversal_0 = count_trees(slope, 1, 1)
    traversal_1 = count_trees(slope, 3, 1)
    traversal_2 = count_trees(slope, 5, 1)
    traversal_3 = count_trees(slope, 7, 1)
    traversal_4 = count_trees(slope, 1, 2)

    product = traversal_0 * traversal_1 * traversal_2 * traversal_3 * traversal_4

    print(product)
    return product


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1])
