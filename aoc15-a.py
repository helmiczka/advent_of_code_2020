def xth_number(numbers, x):
    already_spoken_map = {}
    for i in range(0, len(numbers)):
        already_spoken_map[numbers[i]] = i + 1

    last_spoken_number = numbers[-1]

    for turn in range(len(numbers), x):
        new_number = 0
        if last_spoken_number in already_spoken_map:
            new_number = turn - already_spoken_map[last_spoken_number]

        already_spoken_map[last_spoken_number] = turn
        last_spoken_number = new_number

    return last_spoken_number


def main():
    numbers = [8, 11, 0, 19, 1, 2]
    return xth_number(numbers, 2020)


if __name__ == "__main__":
    print(main())
