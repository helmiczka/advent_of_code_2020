import sys
import re
import copy


def set_mask(line):
    return line.strip()[7:]


def int_to_binary_list(number):
    binary_value = bin(number)
    binary_value = "0" * (38 - len(binary_value)) + binary_value[2:]
    return list(binary_value)


def binary_list_to_int(list):
    return int(''.join(list), 2)


def decode(line, mask):
    regex_match = re.match(r'mem\[(\d+)\] = (\d+)', line)
    address = int(regex_match.groups(0)[0])
    value = int(regex_match.groups(0)[1])

    binary_addresses = [int_to_binary_list(address)]

    for i in range(0, 36):
        if mask[i] == "1":
            for a_list in binary_addresses:
                a_list[i] = mask[i]
        if mask[i] == "X":
            new_addresses = copy.deepcopy(binary_addresses)
            for new_a_list in new_addresses:
                if new_a_list[i] == '0':
                    new_a_list[i] = '1'
                else:
                    new_a_list[i] = '0'
            binary_addresses.extend(new_addresses)

    addresses = [binary_list_to_int(address) for address in binary_addresses]

    return addresses, value


def main(inputfilename):
    memory = {}
    bitmask = ""

    with open(inputfilename, "r") as f:
        for line in f:
            if "mask" in line:
                bitmask = set_mask(line)
            else:
                addresses, value = decode(line, bitmask)
                for a in addresses:
                    memory[a] = value

    return sum(memory.values())


if __name__ == "__main__":
    print(main(sys.argv[1]))
