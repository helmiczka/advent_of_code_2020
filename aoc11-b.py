import sys


def occupied_seat(area, x, y, dir_x, dir_y):
    while 0 <= x + dir_x < len(area[0]) and 0 <= y + dir_y < len(area):
        y += dir_y
        x += dir_x
        if area[y][x] == '#':
            return True
        elif area[y][x] == 'L':
            return False
    return False

def neighbors(area, x, y):
    neighbors_count = 0

    if occupied_seat(area, x, y, -1, 0):
        neighbors_count += 1
    if occupied_seat(area, x, y, 1, 0):
        neighbors_count += 1
    if occupied_seat(area, x, y, 0, -1):
        neighbors_count += 1
    if occupied_seat(area, x, y, 0, 1):
        neighbors_count += 1
    
    if occupied_seat(area, x, y, -1, -1):
        neighbors_count += 1
    if occupied_seat(area, x, y, 1, 1):
        neighbors_count += 1
    if occupied_seat(area, x, y, 1,-1):
        neighbors_count += 1
    if occupied_seat(area, x, y, -1, 1):
        neighbors_count += 1

    return neighbors_count

def apply_rules(area):
    new_area = []
    for y in range(0, len(area)):
        new_area_line = []
        for x in range(0, len(area[0])):
            if area[y][x] == '.':
                new_area_line.append('.')
            if area[y][x] == 'L':
                if neighbors(area, x, y) == 0:
                    new_area_line.append('#')
                else:
                    new_area_line.append('L')
            if area[y][x] == '#':
                if neighbors(area, x, y) >= 5:
                    new_area_line.append('L')
                else:
                    new_area_line.append('#')

        new_area.append(new_area_line)

    return new_area

def count_seats(area):
    seats = 0
    for line in area:
        for element in line:
            if element == '#':
                seats += 1
    return seats

def main(inputfilename):
    area = []
    with open(inputfilename, "r") as f:
        for line in f:
            area_line = []
            for char in line.strip():
                area_line.append(char)
            area.append(area_line)

    previous_area = []
    while previous_area != area:
        previous_area = area.copy()
        area = apply_rules(area)

    occupied_seats = count_seats(area)
    return occupied_seats

if __name__ == "__main__":
    print(main(sys.argv[1]))
