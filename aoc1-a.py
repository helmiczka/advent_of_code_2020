import sys


def main(inputfilename):
    expenses = []

    f = open(inputfilename, "r")
    for line in f:
        expenses.append(int(line))

    for e in expenses:
        if (2020 - e) in expenses:
            result = e * (2020 - e)
            print(result)
            return result


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1])
