import sys
import matplotlib.pyplot as plt
import importlib

day = sys.argv[1]

fig = plt.figure()
fig.suptitle('Day ' + day, fontsize=30, fontweight='bold')

try:
    part1module = importlib.import_module("aoc" + day + "-a")
    part1result = part1module.main('aoc' + day + '.txt')
    plt.figtext(0.1, 0.30, 'Part 1: ' + str(part1result), fontsize=22)

    part2module = importlib.import_module("aoc" + day + "-b")
    part2result = part2module.main('aoc' + day + '.txt')
    plt.figtext(0.1, 0.10, 'Part 2: ' + str(part2result), fontsize=22)
    # figManager = plt.get_current_fig_manager()
    # figManager.full_screen_toggle()
except ImportError:
    plt.figtext(0.1, 0.30, 'Day ' + day + ' has no solution', fontsize=22)

plt.show()
