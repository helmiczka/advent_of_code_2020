import sys


def main(inputfilename):
    instructions = []

    f = open(inputfilename, "r")

    for line in f:
        instruction, argument = line.strip().split()
        instructions.append((instruction, argument))

    accumulator = 0
    visited = []
    i = 0

    while not i in visited:
        visited.append(i)
        if instructions[i][0] == 'nop':
            i += 1
            continue
        if instructions[i][0] == 'acc':
            accumulator += int(instructions[i][1])
            i += 1
            continue
        if instructions[i][0] == 'jmp':
            i += int(instructions[i][1])
            continue

    print(accumulator)
    return accumulator


if __name__ == "__main__":
    main(sys.argv[1])
