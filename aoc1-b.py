import sys
from itertools import combinations


def main(inputfilename):
    expenses = []

    f = open(inputfilename, "r")
    for line in f:
        expenses.append(int(line))

    for sample in list(combinations(expenses, 3)):
        if sample[0] + sample[1] + sample[2] == 2020:
            result = sample[0] * sample[1] * sample[2]
            print(result)
            return result


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1])
