import sys


def boot(instructions):
    accumulator = 0
    visited = []
    i = 0
    succ_boot = False

    while not i in visited:
        visited.append(i)

        if i >= len(instructions):
            succ_boot = True
            break

        if instructions[i][0] == 'nop':
            i += 1
            continue
        if instructions[i][0] == 'acc':
            accumulator += int(instructions[i][1])
            i += 1
            continue
        if instructions[i][0] == 'jmp':
            i += int(instructions[i][1])
            continue

    return accumulator, succ_boot


def modify_instructions(instructions, i):
    modified_instructions = instructions.copy()
    if i[0] == 'nop':
        modified_instructions[modified_instructions.index(i)] = ('jmp', i[1])
    if i[0] == 'jmp':
        modified_instructions[modified_instructions.index(i)] = ('nop', i[1])

    return modified_instructions


def main(inputfilename):
    instructions = []

    f = open(inputfilename, "r")

    for line in f:
        instruction, argument = line.strip().split()
        instructions.append((instruction, argument))

    for i in instructions:  # brute force
        if i[0] == 'nop' or i[0] == 'jmp':
            modified_instructions = modify_instructions(instructions, i)
            accumulator, succ_boot = boot(modified_instructions)
            if succ_boot:
                break

    print(accumulator)
    return accumulator


if __name__ == "__main__":
    main(sys.argv[1])
