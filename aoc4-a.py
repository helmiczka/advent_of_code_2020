import sys


def main(inputfilename):
    passports = []
    valid_passports = 0

    f = open(inputfilename, "r")

    current_passport = {}

    for line in f:
        line = line.split()
        if len(line) == 0:
            passports.append(current_passport)
            current_passport = {}
        else:
            for element in line:
                pair = element.split(':')
                current_passport[pair[0]] = pair[1]
    else:  # append last passport which does not have extra newline
        passports.append(current_passport)

    for passport in passports:
        if ('byr' in passport and
                'iyr' in passport and
                'eyr' in passport and
                'hgt' in passport and
                'hcl' in passport and
                'ecl' in passport and
                'pid' in passport):
            valid_passports += 1

    print(valid_passports)
    return valid_passports


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1])
