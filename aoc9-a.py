import sys
from itertools import combinations


def main(inputfilename):
    numbers = []

    with open(inputfilename, "r") as f:
        for line in f:
            numbers.append(int(line))

    invalid_number = 0

    for i in range(25, len(numbers)):
        all_past_25_sums = [x + y for (x, y) in combinations(numbers[i - 25:i], 2)]
        if numbers[i] not in all_past_25_sums:
            invalid_number = numbers[i]

    return invalid_number


if __name__ == "__main__":
    print(main(sys.argv[1]))
