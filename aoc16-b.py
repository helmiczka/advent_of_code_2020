import sys
import re
from collections import namedtuple

Rule = namedtuple('Rule', 'lower_interval upper_interval')


def read_rules(f):
    rules = []
    while True:
        line = f.readline().strip()
        if not line:
            break
        else:
            regex_match = re.match(r'.+: (\d+)-(\d+) or (\d+)-(\d+)', line)
            intervals = [int(x) for x in regex_match.groups(0)]
            rules.append(Rule(intervals[0:2], intervals[2:4]))

    return rules


def read_single_ticket(line):
    return [int(x) for x in line.split(',')]


def read_my_ticket(f):
    my_ticket = []
    f.readline()
    while True:
        line = f.readline().strip()
        if not line:
            break
        else:
            my_ticket = read_single_ticket(line)

    return my_ticket


def read_nearby_tickets(f):
    nearby_tickets = []
    f.readline()
    while True:
        line = f.readline().strip()
        if not line:
            break
        else:
            nearby_tickets.append(read_single_ticket(line))

    return nearby_tickets


def valid_field(field, rule):
    return (rule.lower_interval[0] <= field <= rule.lower_interval[1]
            or rule.upper_interval[0] <= field <= rule.upper_interval[1])


def valid_ticket(ticket, rules):
    for field in ticket:
        field_is_valid_according_to_at_least_one_rule = False

        for rule in rules:
            if valid_field(field, rule):
                field_is_valid_according_to_at_least_one_rule = True

        if not field_is_valid_according_to_at_least_one_rule:
            return False

    return True


def possible_rules(field, rules):
    possibilities = set()
    for rule in rules:
        if valid_field(field, rule):
            possibilities.add(rules.index(rule))

    return possibilities


def get_rules_applicable_for_fields(nearby_tickets, rules):
    applicable_rules = []

    for field_num in range(0, len(nearby_tickets[0])):
        applicable_rules_for_field = []
        for ticket in nearby_tickets:
            applicable_rules_for_field.append(possible_rules(ticket[field_num], rules))
        intersection = set.intersection(*applicable_rules_for_field)
        # choose only those rules, which can be applied to all tickets -> intersection
        applicable_rules.append(intersection)

    return applicable_rules


def map_fields_to_rules(applicable_rules, rules):
    fields_to_rules = {}

    for i in range(0, len(rules)):
        unambiguously_mapped_field_to_rule = set()

        for rule in applicable_rules:
            if len(rule) == 1:
                unambiguously_mapped_field_to_rule = rule

        field_value = list(unambiguously_mapped_field_to_rule)[0]
        rule_value = applicable_rules.index(unambiguously_mapped_field_to_rule)
        fields_to_rules[field_value] = rule_value

        for rule in applicable_rules:
            rule.discard(field_value)

    return fields_to_rules


def main(inputfilename):
    my_ticket = []

    with open(inputfilename, "r") as f:
        rules = read_rules(f)
        my_ticket = read_my_ticket(f)
        nearby_tickets = read_nearby_tickets(f)

    valid_nearby_tickets = [t for t in nearby_tickets if valid_ticket(t, rules)]

    applicable_rules = get_rules_applicable_for_fields(valid_nearby_tickets, rules)

    fields_to_rules = map_fields_to_rules(applicable_rules, rules)

    first_six_values_multiplied = 1
    for i in range(0, 6):
        first_six_values_multiplied *= my_ticket[fields_to_rules[i]]

    return first_six_values_multiplied


if __name__ == "__main__":
    print(main(sys.argv[1]))
