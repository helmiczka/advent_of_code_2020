import importlib

part1module = importlib.import_module("aoc15-a")


def main():
    numbers = [8, 11, 0, 19, 1, 2]
    return part1module.xth_number(numbers, 30000000)


if __name__ == "__main__":
    print(main())
