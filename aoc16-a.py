import sys
import re


def read_rules(f):
    rules = []
    while True:
        line = f.readline().strip()
        if not line:
            break
        else:
            regex_match = re.match(r'.+: (\d+)-(\d+) or (\d+)-(\d+)', line)
            intervals = [int(x) for x in regex_match.groups(0)]
            rules.append(intervals[0:2])
            rules.append(intervals[2:4])

    return rules


def read_my_ticket(f):
    while True:
        line = f.readline().strip()
        if not line:
            break


def read_nearby_tickets(f):
    nearby_tickets = []
    f.readline()
    while True:
        line = f.readline().strip()
        if not line:
            break
        else:
            nearby_tickets.append([int(x) for x in line.split(',')])

    return nearby_tickets


def validate_ticket(ticket, rules):
    ticket_error_rate = 0
    for field in ticket:
        valid = False
        for rule in rules:
            if rule[0] <= field <= rule[1]:
                valid = True
        if not valid:
            ticket_error_rate += field

    return ticket_error_rate


def main(inputfilename):
    ticket_scanning_error_rate = 0

    with open(inputfilename, "r") as f:
        rules = read_rules(f)
        read_my_ticket(f)
        nearby_tickets = read_nearby_tickets(f)

        for ticket in nearby_tickets:
            ticket_scanning_error_rate += validate_ticket(ticket, rules)

    return ticket_scanning_error_rate


if __name__ == "__main__":
    print(main(sys.argv[1]))
