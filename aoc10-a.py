import sys


def main(inputfilename):
    adapter_chain = []
    charging_outlet = 0

    with open(inputfilename, "r") as f:
        for line in f:
            adapter_chain.append(int(line))

    adapter_chain.append(charging_outlet)
    adapter_chain.sort()

    jolt_1_differences = 0
    jolt_3_differences = 1

    for i in range(1, len(adapter_chain)):
        jolt_difference = adapter_chain[i] - adapter_chain[i - 1]
        if jolt_difference == 1:
            jolt_1_differences += 1
        if jolt_difference == 3:
            jolt_3_differences += 1

    result = jolt_1_differences * jolt_3_differences

    return result


if __name__ == "__main__":
    print(main(sys.argv[1]))
