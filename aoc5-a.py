import sys


def main(inputfilename):
    highest_id = 0

    f = open(inputfilename, "r")

    for line in f:
        row = int(line[0:7].replace("F", "0").replace("B", "1"), 2)
        column = int(line[7:10].replace("L", "0").replace("R", "1"), 2)
        seat_id = row * 8 + column
        if seat_id > highest_id:
            highest_id = seat_id

    print(highest_id)
    return highest_id


if __name__ == "__main__":
    main(sys.argv[1])
