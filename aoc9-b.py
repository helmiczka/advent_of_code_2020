import importlib
import sys

part1module = importlib.import_module("aoc9-a")


def find_contiguous_numbers(numbers, invalid_number):
    sample = []
    for sample_size in range(2, len(numbers)):
        for i in range(0, len(numbers) - sample_size):
            sample = numbers[i:i + sample_size]
            if sum(sample) == invalid_number:
                return sample


def main(inputfilename):
    numbers = []
    invalid_number = part1module.main(inputfilename)

    with open(inputfilename, "r") as f:
        for line in f:
            numbers.append(int(line))

    numbers = numbers[:numbers.index(invalid_number)]  # shorten the array
    contiguous_numbers = find_contiguous_numbers(numbers, invalid_number)

    lowest_in_sample = min(contiguous_numbers)
    highest_in_sample = max(contiguous_numbers)
    return lowest_in_sample + highest_in_sample


if __name__ == "__main__":
    print(main(sys.argv[1]))
