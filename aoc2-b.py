import sys
from operator import xor


def main(inputfilename):
    valid_passwords = 0

    f = open(inputfilename, "r")

    for line in f:
        positions, letter, password = line.split()
        first = int(positions.split('-')[0])
        second = int(positions.split('-')[1])
        letter = letter[0]
        if xor(password[first - 1] == letter, password[second - 1] == letter):
            valid_passwords += 1

    print(valid_passwords)
    return valid_passwords


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1])
