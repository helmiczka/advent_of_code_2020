import sys


def transform(subject_number=7, loop_size=1):
    # print('Transforming ', subject_number, loop_size)
    value = 1
    for i in range(0, loop_size):
        value *= subject_number
        value = value % 20201227
    return value


def reverse_transform(public_key, subject_number=7, loop_size=400000000):
    value = 1
    for i in range(0, loop_size):
        value *= subject_number
        value = value % 20201227
        if value == public_key:
            return i + 1
    return 0


def main(inputfilename):
    with open(inputfilename, "r") as f:
        card_public_key = int(f.readline().strip())
        door_public_key = int(f.readline().strip())

    card_loop_size = reverse_transform(card_public_key)
    door_loop_size = reverse_transform(door_public_key)

    encryption_key = transform(subject_number=card_public_key, loop_size=door_loop_size)
    encryption_key2 = transform(subject_number=door_public_key, loop_size=card_loop_size)

    if encryption_key != encryption_key2:
        print('ERROR keys don\'t match')
        print(encryption_key)
        print(encryption_key2)

    return encryption_key


if __name__ == "__main__":
    print(main(sys.argv[1]))
