import sys


def main(inputfilename):
    ids = []

    f = open(inputfilename, "r")

    for line in f:
        row = int(line[0:7].replace("F", "0").replace("B", "1"), 2)
        column = int(line[7:10].replace("L", "0").replace("R", "1"), 2)
        seat_id = row * 8 + column
        ids.append(seat_id)

    ids.sort()
    i = 0
    while ids[i + 1] - ids[i] != 2:
        i += 1

    my_id = ids[i] + 1

    print(my_id)
    return my_id


if __name__ == "__main__":
    main(sys.argv[1])
