import sys


def valid_height(height):
    if len(height) < 4:
        return False

    num_value = int(height[:-2])
    if 'cm' in height:
        if 150 <= num_value <= 193:
            return True
    elif 'in' in height:
        if 59 <= num_value <= 76:
            return True
    return False


def valid_hcl(hcl):
    if len(hcl) != 7 or hcl[0] != '#':
        return False

    for c in hcl[1:]:
        if not ('0' <= c <= '9' or 'a' <= c <= 'f'):
            return False

    return True


def valid_pid(pid):
    if len(pid) != 9:
        return False

    for c in pid:
        if c < '0' or c > '9':
            return False

    return True


def main(inputfilename):
    passports = []
    valid_passports = 0

    f = open(inputfilename, "r")

    current_passport = {}

    for line in f:
        line = line.split()
        if len(line) == 0:
            passports.append(current_passport)
            current_passport = {}
        else:
            for element in line:
                pair = element.split(':')
                current_passport[pair[0]] = pair[1]
    else:  # append last passport which does not have extra newline
        passports.append(current_passport)

    for passport in passports:
        if ('byr' in passport and 1920 <= int(passport['byr']) <= 2002 and
                'iyr' in passport and 2010 <= int(passport['iyr']) <= 2020 and
                'eyr' in passport and 2020 <= int(passport['eyr']) <= 2030 and
                'hgt' in passport and valid_height(passport['hgt']) and
                'hcl' in passport and valid_hcl(passport['hcl']) and
                'ecl' in passport and passport['ecl'] in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth') and
                'pid' in passport and valid_pid(passport['pid'])):
            valid_passports += 1

    print(valid_passports)
    return valid_passports


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1])
