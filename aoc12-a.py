import sys
from enum import Enum


class Direction(Enum):
    N = 0
    E = 1
    S = 2
    W = 3


def manhattan_distance(x, y):
    return abs(x) + abs(y)


def main(inputfilename):
    x = 0
    y = 0
    facing = Direction.E

    with open(inputfilename, "r") as f:
        for line in f:
            line = line.strip()
            number = int(line[1:])

            if line[0] == "N":
                y += number
            if line[0] == "S":
                y -= number
            if line[0] == "E":
                x += number
            if line[0] == "W":
                x -= number

            if line[0] == "F":
                if facing == Direction.N:
                    y += number
                if facing == Direction.S:
                    y -= number
                if facing == Direction.E:
                    x += number
                if facing == Direction.W:
                    x -= number

            if line[0] == "L":
                facing = Direction((facing.value - number / 90) % len(Direction))
            if line[0] == "R":
                facing = Direction((facing.value + number / 90) % len(Direction))

    return manhattan_distance(x, y)


if __name__ == "__main__":
    print(main(sys.argv[1]))
