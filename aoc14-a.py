import sys
import re


def set_mask(line):
    return line.strip()[7:]


def decode(line, mask):
    regex_match = re.match(r'mem\[(\d+)\] = (\d+)', line)
    address = int(regex_match.groups(0)[0])
    value = int(regex_match.groups(0)[1])

    binary_value = bin(value)
    binary_value = "0" * (38 - len(binary_value)) + binary_value[2:]

    value_list = list(binary_value)

    for i in range(0, 36):
        if mask[i] != "X":
            value_list[i] = mask[i]

    value = int(''.join(value_list), 2)
    return address, value


def main(inputfilename):
    memory = {}

    with open(inputfilename, "r") as f:
        for line in f:
            if "mask" in line:
                mask = set_mask(line)
            else:
                address, value = decode(line, mask)
                memory[address] = value

    sum = 0
    for value in memory.values():
        sum += value

    return sum


if __name__ == "__main__":
    print(main(sys.argv[1]))
