def move_cups(cups, moves):
    current_cup = cups[0]
    for i in range(0, moves):
        cups = rotate_cups(cups, current_cup)
        cups_to_move = cups[1:4]
        rest_of_cups = [cups[0]] + cups[4:]
        current_cup_sorted_position = sorted(rest_of_cups).index(current_cup)
        destination_cup_sorted_position = current_cup_sorted_position - 1
        if destination_cup_sorted_position < 0:
            destination_cup = sorted(rest_of_cups)[-1]
        else:
            destination_cup = sorted(rest_of_cups)[destination_cup_sorted_position]

        rest_of_cups = rotate_cups(rest_of_cups, destination_cup)
        destination_cup_position = rest_of_cups.index(destination_cup)

        cups = [rest_of_cups[0]] + cups_to_move + rest_of_cups[1:]
        cups = rotate_cups(cups, current_cup)
        current_cup = cups[1]
    return cups


def rotate_cups(cups, cup):
    first_cup = cups.index(cup)
    return cups[first_cup:] + cups[:first_cup]


def order_after_cup1(cups):
    cups = rotate_cups(cups, 1)
    return "".join([str(cup) for cup in cups])


def main():
    # example
    # cups = [3, 8, 9, 1, 2, 5, 4, 6, 7]
    # puzzle input
    cups = [2, 1, 9, 7, 4, 8, 3, 6, 5]
    cups = move_cups(cups, 100)
    return order_after_cup1(cups)[1:]


if __name__ == "__main__":
    print(main())
