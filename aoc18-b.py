import sys


def cut_sub_expression(line):
    i = 0
    for i in range(1, len(line)):
        if line[0:i].count("(") == line[0:i].count(")"):
            break
    return i


def solve(line):
    result = 0
    stack = []
    i = 0
    while i < len(line):
        if line[i].isdigit():
            stack.append(int(line[i]))
        elif line[i] == "+" or line[i] == "*":
            stack.append(line[i])
        elif line[i] == "(":
            sub_expression_end = cut_sub_expression(line[i:])
            stack.append(solve(line[i + 1:i + sub_expression_end]))
            i += sub_expression_end

        if len(stack) >= 3:
            if stack[-2] == "+":
                result = stack[-3] + stack[-1]
                stack.pop()
                stack.pop()
                stack[-1] = result
        i += 1
    while len(stack) >= 3:
        if stack[-2] == "*":
            result = stack[-3] * stack[-1]
            stack.pop()
            stack.pop()
            stack[-1] = result

    return result


def main(inputfilename):
    sum = 0

    with open(inputfilename, "r") as f:
        for line in f:
            sum += solve(line.strip())

    return sum


if __name__ == "__main__":
    print(main(sys.argv[1]))
