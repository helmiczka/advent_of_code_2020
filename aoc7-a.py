import sys


def find_paths_for(node, graph):
    start_nodes = set()

    for edge in graph:
        if node in edge[1]:
            start_nodes.add(edge[0])
            start_nodes.update(find_paths_for(edge[0], graph))

    return start_nodes


def main(inputfilename):
    rules = []
    outer_bag_set = set()

    f = open(inputfilename, "r")

    for line in f:
        outer_bag, contained = line.strip().split(' bags contain ')
        contained = contained.split(', ')
        for bag in contained:
            rules.append((outer_bag, bag))

    outer_bag_set = outer_bag_set.union(find_paths_for('shiny gold', rules))

    print(len(outer_bag_set))
    return len(outer_bag_set)


if __name__ == "__main__":
    main(sys.argv[1])
