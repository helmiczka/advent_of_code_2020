import sys


def main(inputfilename):
    valid_passwords = 0

    f = open(inputfilename, "r")

    for line in f:
        times, letter, password = line.split()
        at_least = int(times.split('-')[0])
        max_of = int(times.split('-')[1])
        letter = letter[0]
        occurrence = 0
        for c in password:
            if letter == c:
                occurrence += 1
        if at_least <= occurrence <= max_of:
            valid_passwords += 1

    print(valid_passwords)
    return valid_passwords


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1])
